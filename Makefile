build:
	mvn clean install
deploy:
	terraform init terraform
	terraform apply terraform
destroy:
	terraform destroy terraform