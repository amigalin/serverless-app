variable "region" {
  default = "us-west-1"
}

variable "upload-lambda-name" {
  default = "upload-lambda"
}
variable "upload-lambda-handler" {
  default = "LambdaFunction"
}
variable "upload-lambda-filename" {
  default = "target/serverless-app-1.0.0.jar"
}