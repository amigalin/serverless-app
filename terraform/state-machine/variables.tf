variable "step_function_name" {
  description = "The unique name of your Step Function."
}

variable "lambda_arn" {
  description = "The ARN to be used for invoking Lambda Function"
}

variable "region" {
}
