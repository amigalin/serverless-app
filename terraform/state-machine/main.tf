resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = var.step_function_name
  role_arn = aws_iam_role.sfn-exec.arn

  definition = templatefile("${path.module}/upload-strategy.json",
  { "lambda_arn" : var.lambda_arn })
}


resource "aws_iam_role" "sfn-exec" {
  name = "sfn-exec"
  assume_role_policy = data.aws_iam_policy_document.sfn-assume-role.json
}

data "aws_iam_policy_document" "sfn-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["states.${var.region}.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda-invoke" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "lambda-invoke" {
  name = "lambda-invoke"
  policy = data.aws_iam_policy_document.lambda-invoke.json
}

resource "aws_iam_role_policy_attachment" "lambda-invoke" {
  role       = aws_iam_role.sfn-exec.name
  policy_arn = aws_iam_policy.lambda-invoke.arn
}
