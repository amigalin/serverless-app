output "gateway-url" {
  description = "A unique public URL for your Lambda Function"
  value = "${aws_api_gateway_deployment.api_gateway_deployment.invoke_url}${aws_api_gateway_resource.api_gateway_resource.path}"
}