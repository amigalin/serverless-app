resource "aws_api_gateway_rest_api" "api_gateway" {
  name = "api-gateway-sf"
}

resource "aws_api_gateway_resource" "api_gateway_resource" {
  parent_id = aws_api_gateway_rest_api.api_gateway.root_resource_id
  path_part = "execution"
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
}

resource "aws_api_gateway_method" "api_gateway_method" {
  authorization = "NONE"
  http_method = "POST"
  resource_id = aws_api_gateway_resource.api_gateway_resource.id
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
}


resource "aws_api_gateway_integration" "endpoint_integration" {
  http_method             = aws_api_gateway_method.api_gateway_method.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.region}:states:action/StartExecution"
  credentials = aws_iam_role.sfn-exec-role.arn


  request_templates = {
    "application/json" = <<EOF
{
    "input": "$util.escapeJavaScript($input.json('$'))",
    "stateMachineArn": "${var.invoke_arn}"
}
EOF
  }
  resource_id = aws_api_gateway_resource.api_gateway_resource.id
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
}

resource "aws_api_gateway_method_response" "code200" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  resource_id = aws_api_gateway_resource.api_gateway_resource.id
  http_method = aws_api_gateway_method.api_gateway_method.http_method
  status_code = "200"

}

resource "aws_api_gateway_integration_response" "integration-response" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  resource_id = aws_api_gateway_resource.api_gateway_resource.id
  http_method = aws_api_gateway_method.api_gateway_method.http_method
  status_code = aws_api_gateway_method_response.code200.status_code

}

resource "aws_api_gateway_deployment" "api_gateway_deployment" {
  depends_on = [
    aws_api_gateway_integration.endpoint_integration,
  ]
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  stage_name  = "default"
}



resource "aws_iam_role" "sfn-exec-role" {
  name = "sfn-exec-role"
  assume_role_policy = data.aws_iam_policy_document.sfn-assume-policy.json
}

data "aws_iam_policy_document" "sfn-assume-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["apigateway.amazonaws.com","states.${var.region}.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "sfn-invoke-doc" {
  statement {
    actions = [
      "states:*"
    ]
    resources = [
      "*",
    ]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "sfn-invoke" {
  name = "sfn-invoke-policy"
  policy = data.aws_iam_policy_document.sfn-invoke-doc.json
}

resource "aws_iam_role_policy_attachment" "lambda-invoke" {
  role       = aws_iam_role.sfn-exec-role.name
  policy_arn = aws_iam_policy.sfn-invoke.arn
}
