variable "invoke_arn" {
  description = "The ARN to be used for invoking from API Gateway"
}

variable "region" {
}
