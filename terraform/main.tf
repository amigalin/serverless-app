provider "aws" {
  region = var.region
}

module "upload-lambda" {
  source = "./lambda"
  filename = var.upload-lambda-filename
  policy-document = data.aws_iam_policy_document.upload-lambda-policy.json
  function_name = var.upload-lambda-name
  handler = var.upload-lambda-handler
}

data aws_iam_policy_document "base-lambda-policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"]
    resources = [
      "arn:aws:logs:*:*:*"]
    effect = "Allow"
  }
}

data aws_iam_policy_document "upload-lambda-policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"]
    resources = [
      "arn:aws:logs:*:*:*"]
    effect = "Allow"
  }
  statement {
    actions = [
      "s3:*"]
    resources = [
      "arn:aws:s3:::*"]
    effect = "Allow"
  }
}

module "state-machine" {
  source                = "./state-machine"
  step_function_name = "step-function-upload"
  lambda_arn = module.upload-lambda.arn
  region = var.region
}

module "state-machine-gateway" {
  source                = "./gateway"
  invoke_arn = module.state-machine.upload-state-machine
  region = var.region
}