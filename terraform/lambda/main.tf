resource "aws_lambda_function" "lambda_function" {
  function_name = var.function_name
  handler = var.handler
  role = aws_iam_role.lambda_execution.arn
  runtime = "java11"
  filename = var.filename
  reserved_concurrent_executions = var.reserved_concurrent_executions
  memory_size = var.memory_size
  source_code_hash = filebase64sha256(var.filename)
  tags = var.tags
  timeout = var.timeout

  environment {
    variables = {
      S3_BUCKET = aws_s3_bucket.s3_bucket.id
    }
  }
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.function_name}-bucket"
  acl    = "private"
}

resource "aws_iam_role" "lambda_execution" {
  name = "${var.function_name}-execution-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_policy.json
}

resource "aws_iam_role_policy" "lambda_inline_policy" {
  policy = var.policy-document
  role = aws_iam_role.lambda_execution.name
  name = "${var.function_name}-policy"
}

resource "aws_iam_role_policy_attachment" "lambda-logs" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role = aws_iam_role.lambda_execution.name
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type = "Service"
    }
    effect = "Allow"
  }
}