# Demo app for AWS Step Functions

### Prerequisites

You need to install on your build system

```
[Maven](https://maven.apache.org/) - Dependency Management
JDK 11
```

## Building

Run the following command:
``make build``

## Deployment

Run the following command:
``make deploy``

## Testing

Send POST request to URL from output variable after deploying with following body:
``[
      {
          "url": "http://www.google.com"
      },
      {
          "url": "http://www.amazon.com"
      },
      {
          "url": "https://www.facebook.com"
      }
  ]``
  
## Destroy
 
Run the following command:
 ``make destroy``

