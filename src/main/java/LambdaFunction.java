import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import dao.InputData;
import service.HttpService;


public class LambdaFunction implements RequestHandler<InputData, APIGatewayProxyResponseEvent> {


    @Override
    public APIGatewayProxyResponseEvent handleRequest(InputData event, Context context) {
        context.getLogger().log(String.format("Input data: %s", event.toString()));
        HttpService httpService = new HttpService(context);
        httpService.downloadAndUploadToS3(event.getUrl());
        return new APIGatewayProxyResponseEvent().withStatusCode(200);
    }
}