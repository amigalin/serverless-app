package service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


public class HttpService {

    private final LambdaLogger logger;

    public HttpService(Context context) {
        this.logger = context.getLogger();
    }

    public void downloadAndUploadToS3(String url) {
        URI uri = URI.create(url);
        String bucketName = System.getenv("S3_BUCKET");
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(uri).build();
        String page = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .join();
        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
        try {
            logger.log(String.format("Downloading page from %s and upload to S3 (bucket: %s)", url, bucketName));
            s3Client.putObject(bucketName, uri.getHost(), page);
            logger.log("Upload completed successfully");
        }
        catch(AmazonServiceException e)
        {
            System.err.println(e.getErrorMessage());
        }
    }
}
