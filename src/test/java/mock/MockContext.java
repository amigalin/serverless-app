package mock;

import com.amazonaws.services.lambda.runtime.ClientContext;
import com.amazonaws.services.lambda.runtime.CognitoIdentity;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import java.util.Arrays;

public class MockContext implements Context {

    private static final String AWS_REQUEST_ID = "id";
    private static final  String FUNCTION_NAME = "test_function";
    private static final String LOG_GROUP_NAME = "test_log_group";
    private static final String LOG_STREAM_NAME = "test_log_stream";
    private static final int MEMORY_LIMIT_IN_MB = 128;
    private static final int REMAINING_TIME_IN_MS = 15000;
    private ClientContext clientContext;
    private CognitoIdentity identity;
    private LambdaLogger logger = new TestLogger();

    @Override
    public String getAwsRequestId() {
        return AWS_REQUEST_ID;
    }

    @Override
    public ClientContext getClientContext() {
        return clientContext;
    }

    @Override
    public String getFunctionName() {
        return FUNCTION_NAME;
    }

    @Override
    public String getFunctionVersion() {
        return null;
    }

    @Override
    public String getInvokedFunctionArn() {
        return null;
    }

    @Override
    public CognitoIdentity getIdentity() {
        return identity;
    }

    @Override
    public String getLogGroupName() {
        return LOG_GROUP_NAME;
    }

    @Override
    public String getLogStreamName() {
        return LOG_STREAM_NAME;
    }

    @Override
    public LambdaLogger getLogger() {
        return logger;
    }

    @Override
    public int getMemoryLimitInMB() {
        return MEMORY_LIMIT_IN_MB;
    }

    @Override
    public int getRemainingTimeInMillis() {
        return REMAINING_TIME_IN_MS;
    }

    private static class TestLogger implements LambdaLogger {

        @Override
        public void log(String s) {
            System.err.println(s);
        }

        @Override
        public void log(byte[] bytes) {
            System.err.println(Arrays.toString(bytes));
        }
    }
}