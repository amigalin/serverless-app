import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import dao.InputData;
import mock.MockContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Collections;


class LambdaFunctionTest {

    private static final String MESSAGE = "http://www.google.com";

    @Test
    @Disabled
    void testLambdaFunctionHandler() {
        APIGatewayProxyResponseEvent responseEvent = invokeLambda();
        Assertions.assertEquals(200, responseEvent.getStatusCode());
    }

    private APIGatewayProxyResponseEvent invokeLambda(){
        LambdaFunction lambdaFunction = new LambdaFunction();
        return lambdaFunction.handleRequest(new InputData(), new MockContext());
    }

    @Deprecated
    private APIGatewayProxyRequestEvent createGatewayEvent() {
        return new APIGatewayProxyRequestEvent().withBody(MESSAGE);
    }

    @Deprecated
    private SNSEvent createSnsEvent() {
        SNSEvent.SNS sns = new SNSEvent.SNS();
        SNSEvent.SNSRecord record = new SNSEvent.SNSRecord();
        sns.setMessage(MESSAGE);
        record.setSns(sns);
        SNSEvent snsEvent = new SNSEvent();
        snsEvent.setRecords(Collections.singletonList(record));
        return snsEvent;
    }
}